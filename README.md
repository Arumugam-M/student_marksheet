**To Run this Code Please install the below packages:**
1.npm install
2.npm i fastify

**Once you installed all the above packages**
1.node index.js
Run this above command in your Terminal.

**To Do the CRUD operation using Postman:**
**1.Create/Post Method:**
http://localhost:3000/api/users/Add

**2.Retive/Get Method:**
http://localhost:3000/api/users/Get

**3.Update Method:**
http://localhost:3000/api/users/Update/:StudentId 
For Example: http://localhost:3000/api/users/Update/1

**4.Delete Method:**
http://localhost:3000/api/users/Delete/:StudentId
For Example: http://localhost:3000/api/users/Delete/1
